#ifndef TN3PROTO_HPP
#define TN3PROTO_HPP

#include <cstddef>
#include <winsock2.h>
#include <../include/Structures_TN3.h>  	// tn3balance
//#include <../../include/Structures_TN3.h> 	// tn3realtime

typedef uint8_t   le_uint8_t;
typedef uint16_t  le_uint16_t;
typedef uint32_t  le_uint32_t;

//typedef boost::endian::little_uint8_t   le_uint8_t;
//typedef boost::endian::little_uint16_t  le_uint16_t;
//typedef boost::endian::little_uint32_t  le_uint32_t;


const uint32_t		BEGIN					= 0;
const uint32_t		END						= 1;

const int32_t		MAX_DEVICES_NUMBER		=  16;
const int32_t		MAX_SLOTS_NUMBER		=  10;

const int32_t		MAX_INPUT_BUFFER_SIZE 	= 65536;
const int32_t		CCROM_BUFFER_SIZE		= 32768;
const int32_t		TIMEOUT                 = 1000;
const int32_t		TIMEOUT_DOUBLE          = TIMEOUT * 2;
const int32_t		TIMEOUT_LONG			= TIMEOUT * 5;
const int32_t		MAX_TRANSFER_UNIT 		= 512;

const int32_t		NADC  					= 12;
const int32_t		NAMPL 					= 8;
const int32_t		NILVL					= 8;

const uint32_t		CALIBR_V_TABLE_OFFSET	= 0x0;
const uint32_t		BALANCE_V_TABLE_OFFSET	= 0x1000;
const uint32_t		CALIBR_I_TABLE_OFFSET	= 0x2000;

const TGUID	GUID_DEVICEINTERFACE_COMPORT = {0x86E0D1E0, 0x8089, 0x11D0,
		{0x9C, 0xE4, 0x08, 0x00, 0x3E, 0x30, 0x1F, 0x73}};

const TGUID	GUID_DEVICEINTERFACE_MODEM = {0x2C7089AA, 0x2E0E, 0x11D1,
		{0xB1, 0x14, 0x00, 0xC0, 0xF4, 0xC2, 0xAA, 0xE4}};

typedef struct _MODULE_IDENT {
	std::uint32_t    			u32ID;
	std::uint16_t    			u16Version;
	std::uint16_t    			u16SerialNumber;
} MODULE_IDENT, *LPMODULE_IDENT;

typedef struct _MODULE_STATUS {
	uint32_t  					command;
	uint32_t  					status;
} MODULE_STATUS, *LPMODULE_STATUS;

typedef struct _EXCOMMAND {
	uint32_t  					cmd;
	uint32_t  					params[7];
} EXCOMMAND, *LPEXCOMMAND;

typedef struct _SAMPLES_12xU32 {
	union {
		uint32_t   				u32[12];
		int32_t   				i32[12];
	};
} SAMPLES_12xU32, *LPSAMPLES_12xU32;

typedef struct _SAMPLES_32xU32 {
	uint32_t   					u32[32];
} SAMPLES_32xU32, *LPSAMPLES_32xU32;

typedef struct _CONNECTED_DEVICE {
	SOCKET						so;
	sockaddr					saCtl;
	sockaddr					saDataOut;
	MODULE_IDENT				MID[10];
	uint16_t					wLun;
	uint32_t					uDvcID;
	uint8_t						bOutEnabled;
	uint8_t						Reserved;
} CONNECTED_DEVICE, *LPCONNECTED_DEVICE;

//---------------------------------------------------------------------------
typedef struct _ADC_CHANNEL_STATS {
	int64_t   					iCurrents;
	uint64_t  					qSquares;
} ADC_CHANNEL_STATS, *LPADC_CHANNEL_STATS;

typedef struct _MODULE_ADC_SAMPLES_STATS {
  uint32_t            			uSamplesToCollect;
  uint32_t            			uSamplesCollected;
  ADC_CHANNEL_STATS   			cs[18];
} MODULE_ADC_SAMPLES_STATS, *LPMODULE_ADC_SAMPLES_STATS;

//---------------------------------------------------------------------------
typedef struct _ADC_TABLE_HEADER {
	uint32_t					u32Size;
	uint32_t					u32SizeOfHeader;
	uint16_t					u16Version;
	uint16_t					u16CheckSumm;
	TN3_DATE					Date;
	TN3_TIME					Time;
} ADC_TABLE_HEADER, *LPADC_TABLE_HEADER;

typedef struct _ADC_CALIBR_V_ENTRY {
	int32_t   					b;          // pre-calculated b coeffecient
	int32_t   					k;          // pre-calculated k coeffecient (need scaling)
} ADC_CALIBR_V_ENTRY, *LPADC_CALIBR_V_ENTRY;

// ADC/002
typedef struct _CALIBR_V_TABLE_L12R4 {
	ADC_TABLE_HEADER     		hdr;
	ADC_CALIBR_V_ENTRY			cv[12][4];
} CALIBR_V_TABLE_L12R4, *LPCALIBR_V_TABLE_L12R4;

// ADC/003
typedef struct _CALIBR_V_TABLE_L18R8 {
	ADC_TABLE_HEADER          	hdr;
	ADC_CALIBR_V_ENTRY			cv[18][NAMPL];
} CALIBR_V_TABLE_L18R8, *LPCALIBR_V_TABLE_L18R8;

typedef struct _CALIBR_I_TABLE_I5L2 {
	ADC_TABLE_HEADER			hdr;
	uint16_t   					c[5][2]; 	// Only 2, it's not a bug!
} CALIBR_I_TABLE_I5L2, *LPCALIBR_I_TABLE_I5L2;

// ADC/005, ADC/012
typedef struct _CALIBR_V_TABLE_R4L12 {
	ADC_TABLE_HEADER  			hdr;
	ADC_CALIBR_V_ENTRY			cv[4][NADC];
} CALIBR_V_TABLE_R4L12, *LPCALIBR_V_TABLE_R4L12;

// ADC/006
typedef struct _CALIBR_V_TABLE_R8L12 {
	ADC_TABLE_HEADER  			hdr;
	ADC_CALIBR_V_ENTRY			cv[NAMPL][NADC];
} CALIBR_V_TABLE_R8L12, *LPCALIBR_V_TABLE_R8L12;

typedef struct _BALANCE_ADC_ENTRY {
	uint16_t   					a;       	//
	uint16_t   					k;          // pre-calculated k coeffecient (need scaling)
	int32_t    					iuV;
} BALANCE_ADC_ENTRY, *LPBALANCE_ADC_ENTRY;

typedef struct _BALANCE_TABLE_006 {
	ADC_TABLE_HEADER			hdr;
	BALANCE_ADC_ENTRY   		cv[12];
} BALANCE_TABLE_006, *LPBALANCE_TABLE_006;

typedef struct _CALIBR_I_TABLE_I5L12 {
	ADC_TABLE_HEADER			hdr;
	uint16_t   					c[5][NADC];
} CALIBR_I_TABLE_I5L12, *LPCALIBR_I_TABLE_I5L12;

// ADC/007, ADC/008
typedef struct _CALIBR_V_TABLE_R6L12 {
	ADC_TABLE_HEADER  			hdr;
	ADC_CALIBR_V_ENTRY			cv[6][NADC];
} CALIBR_V_TABLE_R6L12, *LPCALIBR_V_TABLE_R6L12;

// Universal 	ADC/007
typedef struct _CALIBR_I_TABLE_UNIVERSAL {
	ADC_TABLE_HEADER			hdr;
	uint32_t					uNADC;
	uint32_t					uNILVL;
	uint32_t					ideal[NILVL];
	uint32_t   					c[NADC][NILVL];
} CALIBR_I_TABLE_UNIVERSAL, *LPCALIBR_I_TABLE_UNIVERSAL;

//---------------------------------------------------------------------------
// Voltage
typedef CALIBR_V_TABLE_L12R4 	CALIBR_V_TABLE_002;
typedef CALIBR_V_TABLE_L18R8 	CALIBR_V_TABLE_003;

typedef CALIBR_V_TABLE_R4L12	CALIBR_V_TABLE_005;
typedef CALIBR_V_TABLE_R4L12	CALIBR_V_TABLE_012;

typedef CALIBR_V_TABLE_R8L12	CALIBR_V_TABLE_006;
typedef	CALIBR_V_TABLE_R6L12 	CALIBR_V_TABLE_007;
typedef	CALIBR_V_TABLE_R6L12 	CALIBR_V_TABLE_008;

// Current
typedef CALIBR_I_TABLE_I5L2		CALIBR_I_TABLE_003;
typedef CALIBR_I_TABLE_I5L12	CALIBR_I_TABLE_006;
typedef CALIBR_I_TABLE_UNIVERSAL 	CALIBR_I_TABLE_007;
//---------------------------------------------------------------------------

typedef struct _MODULE_INFO {
	MODULE_IDENT				mid;
	int8_t						i8MIBAddr;
	uint8_t						u8Status;
	uint16_t 					u16TaskOffset;
} MODULE_INFO, *LPMODULE_INFO;

typedef struct _UDP_RECORD_MODULES_MAP {
	MODULE_INFO					MI[16];
} UDP_RECORD_MODULES_MAP, *LPUDP_RECORD_MODULES_MAP;

//---------------------------------------------------------------------------
typedef struct _ADC_CALIBR_SAMPLES_LINE {
	union {
		double					f[3];
		struct {
			double				fPos;
			double				fNeg;
			double				fZero;
		};
	};
} ADC_CALIBR_SAMPLES_LINE, *LPADC_CALIBR_SAMPLES_LINE;

typedef struct _ADC_CALIBR_SAMPLES_GAIN
{
	ADC_CALIBR_SAMPLES_LINE		line[18];
} ADC_CALIBR_SAMPLES_GAIN, *LPADC_CALIBR_SAMPLES_GAIN;

typedef struct _ADC_CALIBR_SAMPLES
{
	ADC_CALIBR_SAMPLES_GAIN		gain[8];
} ADC_CALIBR_SAMPLES, *LPADC_CALIBR_SAMPLES;

//
typedef struct _ADC_CALIBR_MEASURED_LINE {
	union {
		bool  					b[3];
		struct {
			bool				bPos;
			bool				bNeg;
			bool				bZero;
		};
	};
} ADC_CALIBR_MEASURED_LINE, *LPADC_CALIBR_MEASURED_LINE;

typedef struct _ADC_CALIBR_MEASURED_GAIN
{
	ADC_CALIBR_MEASURED_LINE	line[18];
} ADC_CALIBR_MEASURED_GAIN, *LPADC_CALIBR_MEASURED_GAIN;

typedef struct _ADC_CALIBR_MEASURED
{
	ADC_CALIBR_MEASURED_GAIN	gain[8];
} ADC_CALIBR_MEASURED, *LPADC_CALIBR_MEASURED;

//#include <boost/endian/arithmetic.hpp>

//namespace tn3
//{

typedef std::uint32_t status_t;

//namespace proto
//{

namespace cmd
{
const uint32_t		READ_CCROM				= 0x0;
const uint32_t		WRITE_CCROM				= 0x1;
const uint32_t		RELOAD_CCROM	   		= 0x3;
const uint32_t		GET_AVG_SAMPLES			= 0x4;
const uint32_t		SET_VOLTAGE_OFFSET		= 0x5;
const uint32_t		SET_POWER_SUPPLY_003	= 0x5;
const uint32_t		SET_POWER_SUPPLY_006	= 0x6;
const uint32_t		SET_POWER_SUPPLY_007	= 0x6;
const uint32_t		SET_GAIN_002			= 0x6;
const uint32_t		SET_GAIN_003			= 0x6;
const uint32_t		SET_GAIN_005			= 0x6;
const uint32_t		SET_GAIN_006			= 0x7;
const uint32_t		SET_GAIN_007			= 0x7;
const uint32_t		SET_GAIN_008			= 0x7;
const uint32_t		SET_GAIN_012			= 0x7;
const uint32_t		SET_VOLTAGE_OFFSET_uV	= 0x8;
const uint32_t		START_SAMPLE_STAT		= 0x10;
const uint32_t		GET_SAMPLE_STAT			= 0x11;
}

namespace error
{
const status_t success                		=  0;
const status_t invalid_packet_size      	= -1;
const status_t invalid_wrapper_signature 	= -1;
const status_t invalid_wrapper_size     	= -1;
const status_t invalid_lun              	= -1;
const status_t invalid_sequence_id      	= -1;
const status_t invalid_sequence_counter 	= -1;
const status_t invalid_transfer_size    	= -1;
const status_t invalid_dbw_size         	= -1;

}

namespace udp
{

    typedef std::uint16_t signature_t;
    typedef std::uint16_t lun_t;
    typedef std::uint16_t seqid_t;
    typedef std::uint16_t seqcnt_t;
    typedef std::uint8_t  request_t;
    typedef std::uint8_t  flags_t;
    typedef std::uint32_t reqsize_t;
    
    const std::size_t COMMAND_BLOCK_WRAPPER_SIZE        = 64;
    const signature_t COMMAND_BLOCK_WRAPPER_SIGNATURE   = 0x4243;
    const std::size_t COMMAND_STATUS_WRAPPER_SIZE       = 64;
    const signature_t COMMAND_STATUS_WRAPPER_SIGNATURE  = 0x5343;
    const std::size_t DATA_BLOCK_WRAPPER_SIZE           = 32;
    const signature_t DATA_BLOCK_WRAPPER_SIGNATURE      = 0x4244;
    const std::size_t DATA_STATUS_WRAPPER_SIZE          = 32;
    const signature_t DATA_STATUS_WRAPPER_SIGNATURE     = 0x5344;
    
    const lun_t       	LUN_ANY							= 0x0000;
    
    const std::uint16_t DEFAULT_PORT                    = 4880;
    
    namespace request
    {
        const request_t   	PING              	= 0x00;
        
        const request_t   	READ_TASK_MEMORY 	= 0x01;
        const request_t   	READ_RECORD_MAP 	= 0x02;
        
        const request_t   	MIB_EXT_RWBUFFER  	= 0x10;
        const request_t   	MIB_EXT_EXECUTE   	= 0x11;
        
        const request_t		MIB_GET_STATUS    	= 0x40;
        const request_t		MIB_GET_ID 	   		= 0x41;
        const request_t		MIB_GET_TASK   		= 0x42;
        
        const request_t		MIB_SET_TASK   		= 0x02;
    }

    typedef struct _PROTO_HEADER
    {
    	le_uint16_t  signature;
    	le_uint16_t  lun;
    	le_uint16_t  seqid;
    	le_uint16_t  seqcnt; // Little endian
    } *LPPROTO_HEADER, PROTO_HEADER;

    typedef struct _CxW_PARAMS {
    	le_uint32_t  u32[12];
    } *LP_CxW_PARAMS, CxW_PARAMS;

    typedef struct _CBW {
    	PROTO_HEADER  proto;
    
    	le_uint32_t   dataTransferLength; // Expected data transfer size by host
    
    	le_uint8_t    requestCode;
    	le_uint8_t    flags;  // 7    -- data stage direction (0 - host->device, 1 - device->host)
    						// 6    -- use BROADCAST_MAC:BROADCAST_IP:SENDER_PORT as target endpoint instead of SENDER_MAC:SENDER_IP:SENDER_PORT
                            // 5..0 -- currently reserved
    	le_uint16_t   reserved;
    
    	CxW_PARAMS    params;
    } *LPCBW, CBW;

typedef struct _CSW {
	PROTO_HEADER  proto;

	le_uint32_t   dataTransferLength; // Exact data transfer size by device
	le_uint32_t   statusCode;

	CxW_PARAMS    params;
} *LPCSW, CSW;

typedef struct _DBW {
	PROTO_HEADER  proto;

	//  dataTransferLength
	//      valid data size in current packet (packet size can
	//      be greater than valid data size, but in most cases
	//      must be equal, except tail data)
	le_uint32_t   dataTransferLength; // Exact data transfer size by device

	// data processing result or protocol handling status
	// (differentiated by code)
	// data stage transfer must be canceled if status is not successful
	le_uint32_t   statusCode;

	le_uint32_t   params[4];
} *LPDBW, DBW;

typedef struct _DSW {
	PROTO_HEADER  proto;

	//  dataTransferLength
	//      ignored (for debug purposes can be residue transfer length)
	le_uint32_t   dataTransferLength;

	// data processing result or protocol handling status
	// (differentiated by code)
	// data stage transfer must be canceled if status is not successful
	le_uint32_t   statusCode;

  	le_uint32_t   params[4];

} *LPDSW, DSW;

} // namespace udp
//} // namespace proto
//} // namespace tn3

#endif // TN3PROTO_HPP
